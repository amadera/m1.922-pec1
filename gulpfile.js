var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('build-css', function() {
    return gulp.src('./sass/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('./'))
});

gulp.task('default', gulp.series('build-css'));